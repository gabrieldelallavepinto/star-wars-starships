import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

import { ChartStarshipsAtributeComponent } from './chart-starships-atribute.component';

describe('ChartStarshipsAtributeComponent', () => {
  let component: ChartStarshipsAtributeComponent;
  let fixture: ComponentFixture<ChartStarshipsAtributeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartStarshipsAtributeComponent ],
      imports: [ProgressSpinnerModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartStarshipsAtributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
