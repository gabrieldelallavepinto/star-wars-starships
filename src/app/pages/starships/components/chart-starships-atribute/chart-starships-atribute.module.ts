import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartStarshipsAtributeComponent } from './chart-starships-atribute.component';
import { ChartsModule } from 'ng2-charts';
import {ProgressSpinnerModule} from 'primeng/progressspinner';

@NgModule({
  declarations: [ChartStarshipsAtributeComponent],
  imports: [
    CommonModule,
    ChartsModule,
    ProgressSpinnerModule
  ],
  exports: [ChartStarshipsAtributeComponent]
})
export class ChartStarshipsAtributeModule { }
