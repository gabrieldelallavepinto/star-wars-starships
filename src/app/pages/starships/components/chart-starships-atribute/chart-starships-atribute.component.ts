import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { StarshipAtributes } from 'src/app/core/interfaces/starship.interface';
import { Starship } from 'src/app/core/models/starship';

@Component({
  selector: 'app-chart-starships-atribute',
  templateUrl: './chart-starships-atribute.component.html',
  styleUrls: ['./chart-starships-atribute.component.sass']
})
export class ChartStarshipsAtributeComponent implements OnInit {

  // All starships
  @Input() starships: Starship[] = [];

  // Selected Starship Atribute
  @Input() starshipAtribute: StarshipAtributes | null = null;

  // Chart configuration
  chartType: ChartType;
  chartData: ChartDataSets[];
  chartOptions: ChartOptions;
  chartLabels: Label[];
  chartColors: Color[];

  constructor() {
    this.chartType = 'bar';
    this.chartData = [];
    this.chartOptions = this.initChartOptions();
    this.chartLabels = [];
    this.chartColors = this.initChartColors();
  }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges) {
    
    if(changes['starships']) {
      this.chartLabels = this.starships.map(starship => starship.name);
    }

    if(changes['starshipAtribute']) {
      this.changeStarshipAtribute(this.starshipAtribute);
    }
  }

  /**
   * Initialize the chart options
   * @returns {ChartOptions}
   */
   initChartOptions(): ChartOptions {
    return {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          ticks: {
            fontColor: '#9b9b9b',
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            fontColor: '#9b9b9b',
            min: 0,
            beginAtZero: true,
          },
          gridLines: {
            color: '#3a3a3a'
          },
        }]
      }
    }
  }

  /**
   * Initialize the chart colors
   * @returns {Color[]}
   */
  initChartColors(): Color[] {
    return [{ 
      backgroundColor: "#ecc72183",
      hoverBackgroundColor: "#ecc721",
      borderColor: "#ecc721",
    }]
  }

  /**
   * Function that changes the attribute of starships
   * @param {StarshipAtributes} value 
   */
  changeStarshipAtribute(starshipAtribute: StarshipAtributes | null): void {

    if(starshipAtribute) {

      // set labels to chart
      this.chartLabels = this.starships.map(starship => starship.name);
      
      // set data to chart
      this.chartData = [{
        label: starshipAtribute,
        data: this.starships.map((starship: Starship) => starship[starshipAtribute]),
        borderWidth: 1,
      }];

    }

  }



}
