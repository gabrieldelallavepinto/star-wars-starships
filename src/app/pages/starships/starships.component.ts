import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Color, Label } from 'ng2-charts';
import { SelectItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { StarshipAtributes } from 'src/app/core/interfaces/starship.interface';
import { Starship } from 'src/app/core/models/starship';
import { StarshipsService } from 'src/app/core/services/starships.service';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class StarshipsComponent implements OnInit {

  // All starships
  starships: Starship[] = [];

  // Options starship Atributes
  optionsStarshipAtributes: SelectItem[];

  // Selected Starship Atribute
  selectedStarshipAtribute: StarshipAtributes | null;

  // Subscriptions
  subscriptionStarships: Subscription = new Subscription();
  subscriptionQueryParams: Subscription = new Subscription();

  constructor(
    private activateRouter: ActivatedRoute,
    private router: Router,
    private starshipsService: StarshipsService
  ) {
    this.optionsStarshipAtributes = this.initOptionsStarshipAtributes();
    this.selectedStarshipAtribute = null;
  }

  ngOnInit(): void {
    this.getStarships();
  }

  /**
   * Initialize the options starshipt atributess
   * @returns {SelectItem[]}
   */
  initOptionsStarshipAtributes(): SelectItem[] {
    return [
      {value: 'crew', label: 'crew'},
      {value: 'costInCredits', label: 'costInCredits'},
      {value: 'cargoCapacity', label: 'cargoCapacity'},
      {value: 'hyperdriveRating', label: 'hyperdriveRating'},
      {value: 'length', label: 'length'},
      {value: 'maxAtmospheringSpeed', label: 'maxAtmospheringSpeed'},
      {value: 'passengers', label: 'passengers'},
    ];
  }

  /**
   * Function that gets the starships
   */
  getStarships(): void {
    this.subscriptionStarships = this.starshipsService.getStarshipsGraphql().subscribe(async (starships: Starship[]) => {
      this.starships = starships;
      
      // you get the attribute parameter
      this.subscriptionQueryParams = this.activateRouter.queryParams.subscribe(params => {
        if(params['atribute']) {
          const atribute = params['atribute'];
          this.selectedStarshipAtribute = atribute;
          this.changeStarshipAtribute(atribute);
        }
      });

    });
  }

  /**
   * Function when changing the dropdown of the starships attribute
   * Add the query param attribute to the path
   * 
   * @param {StarshipAtributes} value
   */
  changeStarshipAtributeDropdown(value: StarshipAtributes): void {
    this.router.navigate(['/starships'], { queryParams: {atribute: value} });
  }


  /**
   * Function that changes the attribute of starships
   * @param {StarshipAtributes} value 
   */
  changeStarshipAtribute(starshipAtribute: StarshipAtributes): void {

    if(starshipAtribute) {

      // sort starships by attribute
      this.starships = this.starships.sort((a,b) => b[starshipAtribute] -a[starshipAtribute]);
    
    }

  }

  /**
   * A callback method that performs custom clean-up
   */
  ngOnDestroy() {
    if(this.subscriptionStarships) this.subscriptionStarships.unsubscribe();
  }

}
