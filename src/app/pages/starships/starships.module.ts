import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StarshipsRoutingModule } from './starships-routing.module';
import { StarshipsComponent } from './starships.component';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { DropdownModule } from 'primeng/dropdown';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { CardModule } from 'primeng/card';
import { ChartStarshipsAtributeModule } from './components/chart-starships-atribute/chart-starships-atribute.module';

@NgModule({
  declarations: [StarshipsComponent],
  imports: [
    CommonModule,

    FormsModule,
    DropdownModule,
    ScrollPanelModule,
    CardModule,
    ChartStarshipsAtributeModule,

    StarshipsRoutingModule
  ]
})
export class StarshipsModule { }
