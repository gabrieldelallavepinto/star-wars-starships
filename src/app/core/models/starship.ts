import { StarshipAtributes, StarshipInterface } from "../interfaces/starship.interface";

export class Starship implements StarshipInterface {
  name: string;
  crew: number;
  costInCredits: number;
  cargoCapacity: number;
  hyperdriveRating: number;
  length: number;
  maxAtmospheringSpeed: number;
  passengers: number;
  
  constructor(
    name: string,
    crew: number,
    costInCredits: number,
    cargoCapacity: number,
    hyperdriveRating: number,
    length: number,
    maxAtmospheringSpeed: number,
    passengers: number,
  ) {
    this.name = name;
    this.crew = crew;
    this.costInCredits = costInCredits;
    this.cargoCapacity = cargoCapacity;
    this.hyperdriveRating = hyperdriveRating;
    this.length = length;
    this.maxAtmospheringSpeed = maxAtmospheringSpeed;
    this.passengers = passengers;
  }

  /**
   * Map data obtained from the database to a Starship class object
   * @param {any} starship - Database value
   * @returns {Starship} Object Starship
   */
   static fromMap(starship: any): Starship {
    return new Starship(
      starship['name'],
      this.fromMapStringValue(starship[StarshipAtributes.crew]),
      starship[StarshipAtributes.costInCredits],
      starship[StarshipAtributes.cargoCapacity],
      starship[StarshipAtributes.hyperdriveRating],
      starship[StarshipAtributes.length],
      starship[StarshipAtributes.maxAtmospheringSpeed],
      this.fromMapStringValue(starship[StarshipAtributes.passengers]),
    )
  }

  /**
   * Map the string value to a number
   * @param {string} value
   * @returns number
   */
  static fromMapStringValue(value: string): number {
    value = value.replace(',', '.');

    let arrayCrewString: string[] = value.split('-');
    
    let arrayCrewNumber = arrayCrewString.map(a => {
      let number = Number(a);
      if(number == NaN) number = 0;
      return number;
    });

    let crew = (arrayCrewNumber.reduce((a, b) => a + b, 0) / arrayCrewNumber.length) || 0;
    return crew;
  }

}