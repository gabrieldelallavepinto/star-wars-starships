export interface StarshipInterface {
    name: string;
    crew: number;
    costInCredits: number;
    cargoCapacity: number;
    hyperdriveRating: number;
    length: number;
    maxAtmospheringSpeed: number;
    passengers: number;
}

/** Attributes of the starship */
export enum StarshipAtributes {
    crew = "crew",
    costInCredits = "costInCredits",
    cargoCapacity = "cargoCapacity",
    hyperdriveRating = "hyperdriveRating",
    length = "length",
    maxAtmospheringSpeed = "maxAtmospheringSpeed",
    passengers = "passengers",
}