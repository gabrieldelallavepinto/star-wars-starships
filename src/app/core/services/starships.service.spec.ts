import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Starship } from '../models/starship';
import { StarshipsService } from './starships.service';

describe('StarshipsService', () => {
  let service: StarshipsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient, HttpHandler, StarshipsService]
    });
    service = TestBed.inject(StarshipsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
