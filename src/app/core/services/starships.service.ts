import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map }  from  'rxjs/operators';
import { Starship } from '../models/starship';
import { GraphQLClient, gql } from 'graphql-request'

@Injectable({
  providedIn: 'root'
})
export class StarshipsService {

  url = environment.swapiGraphql;

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Get Starships by Http Client
   * 
   * @returns {Observable<Starship[]>} starships
   */
  getStarshipsHttpClient(): Observable<Starship[]> {
    const url = environment.swapiGraphql;
    const data = { "query" : "query { allStarships { starships { name crew costInCredits cargoCapacity hyperdriveRating length maxAtmospheringSpeed passengers } } }" };
    const options = {headers: new HttpHeaders({'Content-Type':'application/json'})};

    return this.http.post(url, data, options).pipe(map((res: any) => {
      const starships: any[] = res['data']['allStarships']['starships'];
      return starships.map(starship => Starship.fromMap(starship));
    }));
  }

  getStarshipsGraphql(): Observable<Starship[]> {
    const client = new GraphQLClient(this.url);

    const query = gql`
      {
        allStarships {
          starships {
            name
            crew
            costInCredits
            cargoCapacity
            hyperdriveRating
            length
            maxAtmospheringSpeed
            passengers
          }
        }
      }
    `;

    return new Observable<Starship[]>(subscriber => {
      client.request(query).then( res => {
        const starships: any[] = res['allStarships']['starships'];
        subscriber.next(starships.map(starship => Starship.fromMap(starship)));
      });

    });

  }

}
